try:
    import pandas as pd
    import numpy as np
except ImportError:
    pass

import datetime as dt
from io import StringIO
from csv import DictReader

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

# IMPORTANT: DO NOT DELETE THE COMMENTED CODE WHICH DRIVES SELENIUM, AS I MAY NEED IT IN THE FUTURE

#from selenium import webdriver
#from selenium.webdriver.common.by import By
#from selenium.webdriver.support.ui import WebDriverWait
#from selenium.webdriver.support import expected_conditions as EC

# The following function will run even if pandas and numpy could not be loaded
def get_todays_price(symbol):
    today = dt.date.today()
    period1 = int(dt.datetime.combine(today, dt.datetime.min.time()).timestamp())
    period2 = int(dt.datetime.combine(today + dt.timedelta(1), dt.datetime.min.time()).timestamp())
    data_url = "https://query1.finance.yahoo.com/v7/finance/download/{}?period1={}&period2={}&interval=1d&events=history&includeAdjustedClose=true".format(symbol, period1, period2)    
    requests_session = requests.Session()
    requests_session.mount('https://', HTTPAdapter(max_retries=Retry(connect=3, backoff_factor=0.5)))
    data = StringIO(requests_session.get(data_url).text)
    csv_reader = DictReader(data)
    price = None
    for row in csv_reader:
        if row['Date'] == str(dt.date.today()):
            price = row['Close']
    return price

def get_time_series(asset, start_date, end_date):
    if asset.yahoo_symbol is None: print("No valid symbol for Yahoo! download."); return pd.DataFrame()
    #print("Downloading information from Yahoo! between {} and {}.".format(start_date, end_date))
    
    period1 = int(dt.datetime.combine(start_date, dt.datetime.min.time()).timestamp())
    period2 = int(dt.datetime.combine(end_date, dt.datetime.min.time()).timestamp())
    #url = "https://uk.finance.yahoo.com/quote/{}/history?period1={}&period2={}&interval=1d&filter=history&frequency=1d".format(asset.yahoo_symbol, period1, period2)
    data_url = "https://query1.finance.yahoo.com/v7/finance/download/{}?period1={}&period2={}&interval=1d&events=history&includeAdjustedClose=true".format(asset.yahoo_symbol, period1, period2)
    #chrome_options = webdriver.ChromeOptions()
    #chrome_options.headless=True
    #driver = webdriver.Chrome(executable_path='C:/Users/Mark/.selenium/chromedriver.exe', chrome_options=chrome_options)

    #driver.get(url)
    #try:
    #    agree_button = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//button[@name='agree']")))
    # 
    #     agree_button.click()
    #     cookies = driver.get_cookies()
    #     #data_url = driver.find_element_by_id("Col1-1-HistoricalDataTable-Proxy").find_element_by_link_text("Download data").get_attribute("href")
    #     anchor = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.LINK_TEXT, "Download data")))
    #     data_url = anchor.get_attribute("href")
    # finally: driver.quit()

    requests_session = requests.Session()
    requests_session.mount('https://', HTTPAdapter(max_retries=Retry(connect=3, backoff_factor=0.5)))
    #for cookie in cookies: requests_session.cookies.set(cookie['name'], cookie['value'])
    
    data = StringIO(requests_session.get(data_url).text)
    time_series = pd.read_csv(data) #, converters={'Close': Decimal}) with from decimal import Decimal, but then there's no good way to check which elements of the Series in the dataframe are real valued
    if time_series.empty: return pd.DataFrame()
    time_series = time_series.rename(columns={'Date': 'date', 'Close': 'price'}).set_index('date').rename_axis('data', axis='columns')
    time_series = time_series[['price']]
    time_series = time_series[np.isfinite(time_series.price)]
    time_series['asset_id'] = asset.id
    time_series['source_id'] = 2
    time_series.index = time_series.index.map(lambda string: dt.datetime.strptime(string, '%Y-%m-%d').date())
    return time_series.pivot_table(index=['date', 'asset_id', 'source_id'], values='price')
