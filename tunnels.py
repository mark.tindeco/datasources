from socket import gethostname
from contextlib import nullcontext

def get_port_and_tunnel(ssh_address, ssh_username, exception_host, use_remote_database = False):
    if gethostname() != exception_host and use_remote_database:
        # using the ssh tunnel, after which the database appears on port 3306
        return 3306, ssh_tunnel(ssh_address, ssh_username)
    return 3307, nullcontext()

class ssh_tunnel:

    remote_bind_address = (remote_host_name, remote_host_db_port) = ('localhost', 3307)
    local_bind_address  = (local_host_name,  local_host_db_port)  = ('localhost', 3306)
    
    def __init__(self, ssh_address, ssh_username):
        self.ssh_address = ssh_address
        self.ssh_username = ssh_username
    
    def __enter__(self):
        print("Opening ssh tunnel")
        from sshtunnel import SSHTunnelForwarder # this package may not install on lightweight systems
        self.server = SSHTunnelForwarder(self.ssh_address, ssh_username=self.ssh_username, remote_bind_address=ssh_tunnel.remote_bind_address, local_bind_address=ssh_tunnel.local_bind_address)
        self.server.start()
        
    def __exit__(self, type, value, traceback):
        self.server.stop()
        print("Closed ssh tunnel")
