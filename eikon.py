from datetime import timedelta
import numpy as np
import pandas as pd
import eikon as ek
import time

__all__ = ['eikon']

class eikon:
    # app key can be generated and viewed within Eikon using the command APPKEY.
    # Eikon Desktop or Eikon API Proxy must be running for the following code to work.
    # Note that get_timeseries has a 3000 field limit.
    # For example, if there are 10 instruments then only 300 dates worth of data can be requested at a time.
    app_key_set = False
    
    @classmethod
    def connect(cls):
        application_key = '1bb27bed675e45e3aa94b1511f89574f5cd9d05d'
        #session = ek.DesktopSession(application_key)
        if ~cls.app_key_set:
           # try:
                #session.check_port(9000)
            ek.set_app_key(application_key)
            cls.app_key_set = True
            #except:
            #    raise Exception("Please start Eikon before running this script.")
        #eikon_started = ek.Profile.check_port(session, application_key, 9000)
        #if cls.app_key_set == False:
            #ek.set_app_key(application_key)
            #cls.app_key_set = True

    # this method is unused, but I might want to use it later
    def get_symbology(symbols, from_symbol_type='RIC'):
        eikon.connect()
        return ek.get_symbology(symbols, from_symbol_type=from_symbol_type)
        # from_symbol_type should be in ['RIC', 'ISIN', 'CUSIP', 'SEDOL', 'ticker', 'LipperID', 'IMO', 'OAPermID']
                
    # =============================================================================
    # Queries Eikon for company information
    # =============================================================================
    @staticmethod
    def get_company_info(instruments):
        fields = ['TR.CommonName','TR.CompanyMarketCap.currency','TR.RIC','TR.ISIN','TR.TickerSymbol','CF_EXCHNG','TR.FirstTradeDate', 'TR.InstrumentTypeCode']
        # Other useful fields
        # TR.CompanyMarketCap
        # TR.HeadquartersCountry
        # TR.PrimaryQuote
        # TR.ExchangeName
        # TR.ShortExchangeName
        # CF_EXCHNG
        # TR.TRBCIndustryGroup
        # TR.GICSSector
        # TR.TickerSymbol
        # TR.InstrumentType
        # TR.InstrumentTypeCode
        parameters = None # {'NULL': 'NA'} # {'Frq': 'AM', 'SDate': '2008-01-01', 'EDate': '2018-02-28'}
        field_name = False # 
        raw_output = False # Set this parameter to True to get the data in json format if set to False, the function will return a data frame The default value is False
        debug = False # When set to True, the json request and response are printed.
        eikon.connect()
        company_info, err = ek.get_data(instruments, fields, parameters=parameters, field_name=field_name, raw_output=raw_output, debug=debug)
        time.sleep(1)
        company_info['First Trade Date'] = pd.to_datetime(company_info['First Trade Date'], format="%Y-%m-%d").dt.date
        return company_info.set_index('Instrument')
    
    @staticmethod
    def get_historic_price_chunk(ric, start_date, end_date, adjusted):
        corax = 'adjusted' if adjusted else 'unadjusted'
        end_date = end_date - timedelta(days=1) # don't include the end date
        interval = 'daily'
        fields = ['CLOSE']
        calendar = 'tradingdays'
        eikon.connect()
        try:
            prices = ek.get_timeseries(rics=ric, start_date=str(start_date), end_date=str(end_date), interval=interval, fields=fields, calendar=calendar, corax=corax)
            time.sleep(1)
            prices.columns = ['price']
            price_chunk = prices[np.isfinite(prices['price'])] # remove rows where the price is nan
        except:
            price_chunk = None
        return price_chunk

    # =============================================================================
    # Splits a date interval (start_date, end_date] into intervals of length 2000 which are open on the left and closed on the right.
    # =============================================================================
    def __date_blocks(start_date, end_date):
        number_of_days = (end_date - start_date).days
        if number_of_days < 0: raise ValueError('start date must be earlier than end date!')
        date_blocks = []
        block_size = 2000
        block_start = start_date
        number_of_blocks = 0 if number_of_days == 0 else int(number_of_days / block_size) + 1
        for block_number in range(number_of_blocks):
            block_end = min(block_start + timedelta(days=block_size), end_date)
            date_blocks.append((block_start, block_end))
            block_start = block_end
        return date_blocks

    @staticmethod
    def get_historic_data(ric, asset_id, start_date, end_date, adjusted=False):
        blocks = eikon.__date_blocks(start_date, end_date)
        prices = pd.DataFrame(columns=['price'])
        for date_block in blocks:
            price_chunk = eikon.get_historic_price_chunk(ric, date_block[0], date_block[1], adjusted)
            if price_chunk is not None: prices = pd.concat([prices, price_chunk])
        if prices.empty: return None
        prices.index = prices.index.map(lambda timestamp: timestamp.date())
        prices = prices.rename_axis('date', axis='index').rename_axis('data', axis='columns')
        prices['asset_id'] = asset_id
        prices['source_id'] = 1
        return prices.pivot_table(index=['date', 'asset_id', 'source_id'], values='price')

    @staticmethod
    def get_historic_prices(asset, start_date, end_date, adjusted=False):
        return eikon.get_historic_data(asset.ric, asset.id, start_date, end_date, adjusted)

    @staticmethod
    def dividend_dataframe_raw(ric, start_date, end_date):
        fields = ['TR.DivUnadjustedGross','TR.DivUnadjustedNet','TR.DivCurr','TR.DivExDate','TR.DivRecordDate','TR.DivPayDate','TR.DivTaxStatus']
        # For other useful fields related to dividends, go to the function builder in excel.
        # Select "Data Items" > "Real Time and Fundamental"
        # Select "Category" > "Corporate Actions" > "Dividend History"
        # TR.DivPaymentType                  e.g. Cash Dividend
        # TR.DivType                         Final / Interim
        # TR.DivDate
        # TR.DivOriginalCurr
        # TR.DivEventStatus
        # TR.DivPeriodLength
        # TR.DivPeriodLengthIndicator
        # TR.DivPeriodEndDate
        # TR.DivAnnouncementDate
        # TR.DivRecordDate                   Dividend Record Date
        # TR.DivPayDate                      Dividend Pay Date
        # TR.DivExDate                       Dividend Ex Date
        # TR.DividendFrequency
        # TR.DivSourceOfFunds
        # TR.DivMandatoryVoluntaryIndicator
        # TR.DivTaxStatus                    Dividend Tax Status
        # TR.DivMktExDate
        # TR.DivIsReinvestmentAvailable
        # TR.DivReinvestmentPrice
        # TR.DivReinvestmentDeadline
        # TR.DivCurr                         Currency
        # TR.DivUnadjustedGross              Unadjusted Gross Dividend
        # TR.DivUnadjustedNet                Unadjusted Net Dividend
        # TR.DivAdjustedGross                Adjusted Gross Dividend
        # TR.DivAdjustedNet
        # TR.DivMktRecordDate
        # TR.DivNotes

        parameters = {'DateType': 'XD', 'SDate':str(start_date), 'EDate':str(end_date)} # .strftime('%Y-%m-%d')
        field_name = False # 
        raw_output = False # Set this parameter to True to get the data in json format if set to False, the function will return a data frame The default value is False
        debug = False # When set to True, the json request and response are printed.
        eikon.connect()
        dividend_dataframe, err = ek.get_data(ric, fields, parameters=parameters, field_name=field_name, raw_output=raw_output, debug=debug)
        time.sleep(1)
        return dividend_dataframe

    @staticmethod
    def dividend_dataframe_processed(isin, ric, start_date, end_date):
        dividend_dataframe = eikon.dividend_dataframe_raw(ric, start_date, end_date)
        start_date = start_date + timedelta(days=1) # don't include the start_date
        dividend_dataframe.drop_duplicates(inplace=True)
        # errors='coerce' parses invalid dates into NaT 
        dividend_dataframe['Dividend Ex Date'] = pd.to_datetime(dividend_dataframe['Dividend Ex Date'], format='%Y-%m-%d', errors='coerce').dt.date
        dividend_dataframe['Dividend Record Date'] = pd.to_datetime(dividend_dataframe['Dividend Record Date'], format='%Y-%m-%d', errors='coerce').dt.date
        dividend_dataframe['Dividend Pay Date'] = pd.to_datetime(dividend_dataframe['Dividend Pay Date'], format='%Y-%m-%d', errors='coerce').dt.date
        dividend_dataframe = dividend_dataframe.dropna(subset=['Dividend Ex Date']) # remove rows where the 'Dividend Ex Date' was not a valid date
        dividend_dataframe = dividend_dataframe[np.isfinite(dividend_dataframe['Gross Dividend Amount'])] # remove rows where the 'Gross Dividend Amount' is nan
        if len(dividend_dataframe) == 0: print("No dividend information for " + ric)
        return dividend_dataframe

    @staticmethod
    def corax_dataframe_raw(ric, start_date, end_date):
        start_date = start_date + timedelta(days=1) # don't include the start_date
        fields = ['TR.AdjmtFactorAdjustmentDate', 'TR.AdjmtFactorAdjustmentFactor', 'TR.AdjmtFactorAdjustmentType']
        # 'CAAdjustmentFactor', 'CAMarketAdjustmentFactor'
        parameters = {'SDate':str(start_date), 'EDate':str(end_date)}
        field_name = False # 
        raw_output = False # Set this parameter to True to get the data in json format if set to False, the function will return a data frame The default value is False
        debug = False # When set to True, the json request and response are printed.
        eikon.connect()
        corporate_actions, err = ek.get_data(ric, fields, parameters=parameters, field_name=field_name, raw_output=raw_output, debug=debug)
        time.sleep(1)
        return corporate_actions
    
    @staticmethod
    def corax_dataframe_processed(ric, start_date, end_date):
        start_date = start_date + timedelta(days=1) # don't include the start_date
        corporate_actions = eikon.corax_dataframe_raw(ric, start_date, end_date)
        corporate_actions.rename(columns={'Corporate Action Adjustment Factor Date': 'date', 'Corporate Action Adjustment factor': 'adjustment', 'Corporate Action Adjustment Type': 'adjustment_type'}, inplace=True)
        corporate_actions = corporate_actions[np.isfinite(corporate_actions['adjustment'])] # remove rows where the 'Corporate Action Adjustment factor' is nan
        corporate_actions['date'] = pd.to_datetime(corporate_actions['date'], errors='coerce').dt.date
        corporate_actions = corporate_actions.dropna(subset=['date']) # remove rows where the 'Corporate Action Adjustment Factor Date' was not a valid date
        corporate_actions.set_index(['date', 'Instrument'], inplace=True)
        time.sleep(0.2)
        if len(corporate_actions) == 0: print("No corporate action information for " + ric)        
        return corporate_actions
