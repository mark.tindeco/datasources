from mysql.connector import connect
try:
    import pandas as pd
    import numpy as np
except ImportError:
    pass

__all__ = ['Database', 'DatabaseFinancial']

class Database:
    
    def __init__(self, database, port, host='localhost', db_user='root', db_password=''):
        self.host = host
        self.database = database
        self.port = port
        self.db_user = db_user
        self.db_password = db_password
            
    def __enter__(self):
        print("Opening database connection.")
        self.mariadb_connection = connect(user=self.db_user, password=self.db_password, host=self.host, port=self.port, database=self.database)
        self.cursor = self.mariadb_connection.cursor(buffered=True, dictionary=True)
        return self
           
    def __exit__(self, type, value, traceback):
        if traceback is not None: print("There was an unhandled exception")
        self.cursor.close()
        self.mariadb_connection.commit()
        self.mariadb_connection.close()
        print("Closed database connection.")
        
    def __string__(self):
        return self.database
        
    def execute(self, query):
        try: self.cursor.execute(query)
        except: raise Exception("Failed to execute query " + query)

    def clean_dictionary(input_dictionary):
        if not input_dictionary: return None
        dict_keys = [*input_dictionary.keys()]
        for key in dict_keys:
            value = input_dictionary[key]
            if value is None: del input_dictionary[key]; continue
            if isinstance(value, str): input_dictionary[key] = value.replace("'", "\\'")
        return input_dictionary

    def fetch_result(self, query, field=None):
        self.execute(query)
        result = self.cursor.fetchone()
        if result is not None and field is not None:
            return result[field]
        return result

    def fetch_results(self, query, num=None):
        self.execute(query)
        results = self.cursor.fetchall()
        if num is None: return results
        return results[:num]

    def refresh_row(self, table, index_dictionary):
        index_dictionary = Database.clean_dictionary(index_dictionary)
        if not index_dictionary: return # tests for either None or {}
        set_clause = " SET " + ', '.join(["`%s`='%s'" % (key, value) for (key, value) in index_dictionary.items()])
        query = "REPLACE INTO `{}`".format(table) + set_clause
        self.execute(query)

    def update_table_without_commit(self, table, index_dictionary, values_dictionary):
        index_dictionary = Database.clean_dictionary(index_dictionary)
        if not index_dictionary: return # tests for either None or {}
        values_dictionary = Database.clean_dictionary(values_dictionary)
        if not values_dictionary: return # tests for either None or {}
        query = "UPDATE `{}`".format(table)
        set_clause = " SET " + ', '.join(["`%s`= '%s'" % (key, value) for (key, value) in values_dictionary.items()]) 
        where_clause = " WHERE " + ' AND '.join(["`%s`= '%s'" % (key, value) for (key, value) in index_dictionary.items()])             
        query = query + set_clause + where_clause
        self.execute(query)

    def commit(self):
        self.mariadb_connection.commit()

class DatabaseFinancial(Database):
    def get_or_create_asset(self, symbols):
        # Try and find the asset in the database
        dict_symbols = {}
        ric = symbols['RIC']
        if isinstance(ric, str):
            dict_symbols['RIC'] = ric
            result = self.fetch_result("SELECT `asset_id` FROM `assets` WHERE `RIC`='{}'".format(ric))
            if result is not None: return result['asset_id']
        reuters_id = symbols['OAPermID']
        if isinstance(reuters_id, str): dict_symbols['reuters_id'] = reuters_id
        isin = symbols['ISIN']
        if isinstance(isin, str): dict_symbols['ISIN'] = isin
        cusip = symbols.get('CUSIP', None)
        if isinstance(cusip, str): dict_symbols['CUSIP'] = cusip
        sedol = symbols.get('SEDOL', None)
        if isinstance(sedol, str): dict_symbols['SEDOL'] = sedol
        ticker = symbols.get('ticker', None)
        if isinstance(ticker, str): dict_symbols['ticker'] = ticker
        # Create the asset
        query = "INSERT INTO `assets`"
        set_clause = " SET " + ', '.join(["`%s`= '%s'" % (key, value) for (key, value) in dict_symbols.items()])
        self.execute(query + set_clause)
        asset_id = self.fetch_result("SELECT `asset_id` FROM `assets` WHERE `reuters_id`='{}'".format(reuters_id))['asset_id']
        return asset_id
    
    def get_asset_ids(self, rics=None, isins=None, symbols=None):
        if rics is not None: identifiers, identifier_string = rics, 'ric'
        elif isins is not None: identifiers, identifier_string = isins, 'isin'
        elif symbols is not None: identifiers, identifier_string = symbols, 'symbol'
        else: raise ValueError("Exactly one type of identifier must be used.")
        asset_ids = []
        for identifier in identifiers:
            results = self.fetch_results("SELECT `asset_id` FROM `assets` WHERE `{}`='{}'".format(identifier_string, identifier))
            len_results = len(results)
            if len_results == 0: asset_id = None
            elif len_results == 1: asset_id = results[0]['asset_id']
            else: raise ValueError("Asset could not be uniquely identified using the {} {}.".format(identifier_string, identifier))
            asset_ids.append(asset_id)
        return asset_ids
    
    def get_daily_close_prices(self, asset_ids, start_date=None, end_date=None, wide=False, adjusted=False):
        # Long format means one observation per row, wide format may have multiple observations per row.
        if type(asset_ids) == int: asset_ids = [asset_ids]
        asset_ids = np.asarray([asset_id for asset_id in asset_ids if asset_id is not None])
        query = "SELECT `asset_id`, `date`, `source_id`, `close` AS `price` FROM `daily_close_prices` WHERE `asset_id` IN ('{}')".format('\', \''.join(str(asset_id) for asset_id in asset_ids))
        if start_date is not None: query += " AND `date`>'{}'".format(start_date.strftime('%Y-%m-%d'))
        if end_date is not None: query += " AND `date`<='{}'".format(end_date.strftime('%Y-%m-%d'))
        query += " ORDER BY `date` ASC"
        daily_close_prices = pd.DataFrame(self.fetch_results(query)).rename_axis('data', axis='columns')
        if adjusted:
            query = "SELECT `asset_id`, `date`, `adjustment` FROM `adjustments` WHERE `asset_id` IN ('{}')".format('\', \''.join(str(asset_id) for asset_id in asset_ids))
            if start_date is not None: query += " AND `date`>'{}'".format(start_date.strftime('%Y-%m-%d'))
            if end_date is not None: query += " AND `date`<='{}'".format(end_date.strftime('%Y-%m-%d'))
            query += " ORDER BY `date` ASC"
            adjustments = pd.DataFrame(self.fetch_results(query)).rename_axis('data', axis='columns')
            for index, row in adjustments.iterrows():
                asset_id, date, adjustment = row[['asset_id', 'date', 'adjustment']]
                daily_close_prices['price'] = daily_close_prices['price'].mask((daily_close_prices['asset_id'] == asset_id) & (daily_close_prices['date'] < date), daily_close_prices['price'] * adjustment)
        if daily_close_prices.empty: return None
        if wide: return daily_close_prices.pivot_table(index=['date'], columns=['asset_id', 'source_id'], values='price')
        else: return daily_close_prices.pivot_table(index=['date', 'asset_id', 'source_id'], values='price')

    def save_daily_close_prices(self, daily_close_prices):
        # Assumes daily_close_prices is in long format (see the method get_daily_close_prices)
        if daily_close_prices is None: return
        for index, row in daily_close_prices.iterrows():
            date, asset_id, source_id = index
            close = row['price']
            try:
                self.execute("INSERT INTO `daily_close_prices` SET `asset_id`={}, `date`='{}', `source_id`={}, `close`={}".format(asset_id, date, source_id, close))
            except:
                db_close = self.fetch_result("SELECT `close` FROM `daily_close_prices` WHERE `asset_id`={} AND `date`='{}' AND `source_id`={}".format(asset_id, date, source_id))['close']
                if db_close != close:
                    print("Warning for {}: New close price = {}, database close price = {}.".format(date, close, db_close))

    def save_company(self, info):
        issuer = info['issuer'].replace("'", "\\'")
        query = "INSERT INTO `assets` SET `ISIN`='{}', `symbol`='{}', `RIC`='{}', `name`='{}', `first_trade_date`='{}'".format(info['isin'], info['symbol'], info['ric'], issuer, info['first trade date'])
        asset_type_id = self.asset_type_mapper.get(info['asset_type'], None)
        if asset_type_id is not None: query += ", `asset_type_id`={}".format(asset_type_id)
        exchange_id = self.exchange_mapper.get(info['exchange'], None)
        if exchange_id is not None: query += ", `exchange_id`={}".format(exchange_id)
        currency_id = self.currency_mapper.get(info['currency'], None)
        if currency_id is not None: query += ", `currency_id`={}".format(currency_id)
        self.execute(query)

    def update_company(self, info):
        currency_mapper = self.currency_mapper
        exchange_mapper = self.exchange_mapper
        for asset_id, asset_data in info.iterrows():
            name = asset_data['Company Common Name']
            if isinstance(name, str):
                existing_name = self.fetch_result("SELECT `name` FROM `assets` WHERE `asset_id`=" + str(asset_id))['name']
                if name != existing_name:
                    print("Changing company name from {} to {}".format(existing_name, name))
                    self.execute("UPDATE `assets` SET `name`='{}' WHERE `asset_id`={}".format(name.replace("'", "\'"), asset_id))
            currency = asset_data['Currency']
            if isinstance(currency, str):
                currency_id = currency_mapper.get(currency, None)
                existing_currency_id = self.fetch_result("SELECT `currency_id` FROM `assets` WHERE `asset_id`=" + str(asset_id))['currency_id']
                if currency_id != existing_currency_id and currency_id is not None:
                    print("Changing currency id from {} to {}".format(existing_currency_id, currency_id))
                    self.execute("UPDATE `assets` SET `currency_id`='{}' WHERE `asset_id`={}".format(currency_id, asset_id))
            exchange = asset_data['CF_EXCHNG']
            exchange_id = exchange_mapper.get(exchange, None)
            existing_exchange_id = self.fetch_result("SELECT `exchange_id` FROM `assets` WHERE `asset_id`=" + str(asset_id))['exchange_id']
            if exchange_id != existing_exchange_id:
                print("Changing exchange id from {} to {}".format(existing_exchange_id, exchange_id))
                if exchange_id is None: self.execute("UPDATE `assets` SET `exchange_id`=NULL WHERE `asset_id`={}".format(asset_id))
                else: self.execute("UPDATE `assets` SET `exchange_id`='{}' WHERE `asset_id`={}".format(exchange_id, asset_id))
            first_trade_date = asset_data['First Trade Date']
            existing_first_trade_date = self.fetch_result("SELECT `first_trade_date` FROM `assets` WHERE `asset_id`=" + str(asset_id))['first_trade_date']
            if first_trade_date != existing_first_trade_date and first_trade_date is not None:
                print("Changing first trade date from {} to {}".format(existing_first_trade_date, first_trade_date))
                self.execute("UPDATE `assets` SET `first_trade_date`='{}' WHERE `asset_id`={}".format(first_trade_date, asset_id))
                

    def update_start_date(self, asset_id, start_date):
        query = "UPDATE `assets` SET `start_date`='{}' WHERE `asset_id`={}".format(start_date, asset_id)
        self.execute(query)
        
    def update_end_date(self, asset_id, end_date):
        query = "UPDATE `assets` SET `end_date`='{}' WHERE `asset_id`={}".format(end_date, asset_id)
        self.execute(query)

    @property
    def currency_mapper(self): return pd.DataFrame(self.fetch_results("SELECT * FROM `lookup_currencies`")).set_index('code')['currency_id'].to_dict()
    
    @property
    def exchange_mapper(self): return pd.DataFrame(self.fetch_results("SELECT * FROM `lookup_exchanges`")).set_index('exchange_code')['exchange_id'].to_dict()

    @property
    def asset_type_mapper(self): return pd.DataFrame(self.fetch_results("SELECT * FROM `lookup_asset_type`")).set_index('asset_type_code')['asset_type_id'].to_dict()
